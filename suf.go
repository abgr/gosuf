package suf

import (
	"encoding/binary"
	"errors"
	"syscall"
	"time"
)

type packetType = byte

const (
	packetTypeIgnore packetType = iota
	forwardInfoReq
	forwardInfoRes
	forwardToAddr
	registerNameReq
)

type SUF struct {
	sendTo      func(packet []byte, socketFd int, to syscall.SockaddrInet4) error
	addContact  func(addr syscall.SockaddrInet4, OOB []byte) (DynamicID [8]byte, err error)
	pingContact func(addr syscall.SockaddrInet4, DynamicID [8]byte, OOB []byte) (err error)
	getContact  func(DynamicID [8]byte) (addr syscall.SockaddrInet4, err error)
}

func (suf *SUF) handleForwardInfoPacket(packet []byte, socketFd int, from syscall.SockaddrInet4) error {
	if len(packet) != 1+8 {
		return errors.New("suf: invalid packet")
	}
	resPacket := make([]byte, 1+8+4+2+1+8) //B
	resPacket[0] = forwardInfoRes
	copy(resPacket[1:1+8], packet[1:1+8])
	copy(resPacket[1+8:1+8+4], from.Addr[:])
	binary.BigEndian.PutUint16(resPacket[1+8+4:1+8+4+2], uint16(from.Port))
	resPacket[15] = suf.pressure()
	binary.BigEndian.PutUint64(resPacket[1+8+4+2+1:1+8+4+2+1+8], uint64(time.Now().Unix()))
	return suf.sendTo(resPacket, socketFd, from)
}
func (suf *SUF) handleRegisterName(packet []byte, socketFd int, from syscall.SockaddrInet4) error {
	//xxx
}
func (suf *SUF) handleForwardToAddr(packet []byte, socketFd int) error {
	if len(packet) <= 1+4+2 {
		return errors.New("suf: invalid packet")
	}
	addr := syscall.SockaddrInet4{}
	copy(addr.Addr[:], packet[1:1+4])
	addr.Port = int(binary.BigEndian.Uint16(packet[1+4 : 1+4+2]))
	packet = packet[1+4+2:]
	return suf.sendTo(packet, socketFd, addr)
}
func (suf *SUF) pressure() uint8 {
	return 0
}
func (suf *SUF) input(packet []byte, socketFd int, from syscall.SockaddrInet4) error {
	if len(packet) == 0 {
		return errors.New("suf: empty packet")
	}
	switch packet[0] {
	case packetTypeIgnore, forwardInfoRes:
		return nil
	case forwardInfoReq:
		return suf.handleForwardInfoPacket(packet, socketFd, from)
	case forwardToAddr:
		return suf.handleForwardToAddr(packet, socketFd)
	case registerNameReq:
		suf.handleRegisterName(packet, socketFd, from)
	default:
		return errors.New("suf: unknown packet type")
	}

}
